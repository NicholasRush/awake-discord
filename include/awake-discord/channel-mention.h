#pragma once

#include <awake-discord/enums.h>

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#channel-mention-object-channel-mention-structure
    struct ChannelMention {

        std::string id;

        std::string guild_id;

        ChannelType type;

        std::string name;

        ChannelMention() = default;

        ChannelMention(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            guild_id = json["guild_id"].get<std::string>();
            type = ChannelType(json["type"].get<int>());
            name = json["name"].get<std::string>();
        }

    };

}