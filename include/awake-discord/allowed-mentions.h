#pragma once

#include <awake-discord/string-enums.h>

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#allowed-mentions-object-allowed-mentions-structure
    struct AllowedMentions {

        AllowedMentionTypeFlags parse;

        std::vector<std::string> roles;

        std::vector<std::string> users;

        bool replied_user;

        AllowedMentions() = default;

        AllowedMentions(const nlohmann::json &json) {
            std::vector<std::string> parseValues;
            for (const nlohmann::json &jsonEl : json["parse"]) {
                parseValues.push_back(jsonEl.get<std::string>());
            }
            parse = AllowedMentionTypeFlags_Values::fromStringsVector(parseValues);
            for (const nlohmann::json &jsonEl : json["roles"]) {
                roles.push_back(jsonEl.get<std::string>());
            }
            for (const nlohmann::json &jsonEl : json["users"]) {
                users.push_back(jsonEl.get<std::string>());
            }
            replied_user = json["replied_user"].get<bool>();
        }

    };

}