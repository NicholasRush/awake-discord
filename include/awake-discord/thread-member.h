#pragma once

#include <awake-discord/iso8601.h>

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#thread-member-object-thread-member-structure
    struct ThreadMember {

        std::optional<std::string> id;

        std::optional<std::string> user_id;

        time_t join_timestamp;

        int flags;

        ThreadMember() = default;

        ThreadMember(const nlohmann::json &json) {
            if (json.contains("id")) {
                id = json["id"].get<std::string>();
            }
            if (json.contains("user_id")) {
                user_id = json["user_id"].get<std::string>();
            }
            join_timestamp = ISO8601::fromTimestamp(json["join_timestamp"].get<std::string>());
            flags = json["flags"].get<int>();
        }

    };

}