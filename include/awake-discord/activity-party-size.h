#pragma once

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-party
    struct ActivityPartySize {

        int current_size;

        int max_size;

        ActivityPartySize() = default;

        ActivityPartySize(const nlohmann::json &json) {
            current_size = json["current_size"].get<int>();
            max_size = json["max_size"].get<int>();
        }

    };

}