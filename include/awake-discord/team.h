#pragma once

#include <awake-discord/team-member.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/teams#data-models-team-object
    struct Team {

        std::optional<std::string> icon;

        std::string id;

        std::vector<TeamMember> members;

        std::string name;

        std::string owner_user_id;

        Team() = default;

        Team(const nlohmann::json &json) {
            if (!json["icon"].is_null()) {
                icon = json["icon"].get<std::string>();
            }
            id = json["id"].get<std::string>();
            for (const nlohmann::json &jsonEl : json["members"]) {
                members.emplace_back(jsonEl);
            }
            name = json["name"].get<std::string>();
            owner_user_id = json["owner_user_id"].get<std::string>();
        }

    };

}