#pragma once

#include <nlohmann/json.hpp>
#include <optional>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/permissions#role-object-role-tags-structure
    struct RoleTags {

        std::optional<std::string> bot_id;

        std::optional<std::string> integration_id;

        bool premium_subscriber;

        RoleTags() = default;

        RoleTags(const nlohmann::json &json) {
            if (json.contains("bot_id")) {
                bot_id = json["bot_id"].get<std::string>();
            }
            if (json.contains("integration_id")) {
                integration_id = json["integration_id"].get<std::string>();
            }
            premium_subscriber = json.contains("premium_subscriber");
        }

    };

}