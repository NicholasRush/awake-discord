#pragma once

#include <awake-discord/guild.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/webhook#webhook-object-webhook-structure
    struct Webhook {

        std::string id;

        WebhookType type;

        std::optional<std::string> guild_id;

        std::optional<std::string> channel_id;

        std::optional<User> user;

        std::optional<std::string> name;

        std::optional<std::string> avatar;

        std::optional<std::string> token;

        std::optional<std::string> application_id;

        std::optional<Guild> source_guild;

        std::optional<Channel> source_channel;

        std::optional<std::string> url;

        Webhook() = default;

        Webhook(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            type = WebhookType(json["type"].get<int>());
            if (json.contains("guild_id") && !json["guild_id"].is_null()) {
                guild_id = json["guild_id"].get<std::string>();
            }
            if (!json["channel_id"].is_null()) {
                channel_id = json["channel_id"].get<std::string>();
            }
            if (json.contains("user")) {
                user = User(json["user"]);
            }
            if (!json["name"].is_null()) {
                name = json["name"].get<std::string>();
            }
            if (!json["avatar"].is_null()) {
                avatar = json["avatar"].get<std::string>();
            }
            if (json.contains("token")) {
                token = json["token"].get<std::string>();
            }
            if (!json["application_id"].is_null()) {
                avatar = json["application_id"].get<std::string>();
            }
            if (json.contains("source_guild")) {
                source_guild = Guild(json["source_guild"]);
            }
            if (json.contains("source_channel")) {
                source_channel = Channel(json["source_channel"]);
            }
            if (json.contains("url")) {
                url = json["url"].get<std::string>();
            }
        }

    };

}