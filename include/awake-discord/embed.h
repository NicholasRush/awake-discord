#pragma once

#include <awake-discord/embed-footer.h>
#include <awake-discord/embed-image.h>
#include <awake-discord/embed-thumbnail.h>
#include <awake-discord/embed-video.h>
#include <awake-discord/embed-provider.h>
#include <awake-discord/embed-author.h>
#include <awake-discord/embed-field.h>
#include <awake-discord/string-enums.h>
#include <awake-discord/iso8601.h>

#include <vector>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#embed-object-embed-structure
    struct Embed {

        std::optional<std::string> title;

        std::optional<EmbedType> type;

        std::optional<std::string> description;

        std::optional<std::string> url;

        std::optional<time_t> timestamp;

        std::optional<int> color;

        std::optional<EmbedFooter> footer;

        std::optional<EmbedImage> image;

        std::optional<EmbedThumbnail> thumbnail;

        std::optional<EmbedVideo> video;

        std::optional<EmbedProvider> provider;

        std::optional<EmbedAuthor> author;

        std::vector<EmbedField> fields;

        Embed() = default;

        Embed(const nlohmann::json &json) {
            if (json.contains("title")) {
                title = json["title"].get<std::string>();
            }
            if (json.contains("type")) {
                type = EmbedType_Values::fromString(json["type"].get<std::string>());
            }
            if (json.contains("description")) {
                description = json["description"].get<std::string>();
            }
            if (json.contains("url")) {
                url = json["url"].get<std::string>();
            }
            if (json.contains("timestamp")) {
                timestamp = ISO8601::fromTimestamp(json["timestamp"].get<std::string>());
            }
            if (json.contains("color")) {
                color = json["color"].get<int>();
            }
            if (json.contains("footer")) {
                footer = EmbedFooter(json["footer"]);
            }
            if (json.contains("image")) {
                image = EmbedImage(json["image"]);
            }
            if (json.contains("thumbnail")) {
                thumbnail = EmbedThumbnail(json["thumbnail"]);
            }
            if (json.contains("video")) {
                video = EmbedVideo(json["video"]);
            }
            if (json.contains("provider")) {
                provider = EmbedProvider(json["provider"]);
            }
            if (json.contains("author")) {
                author = EmbedAuthor(json["author"]);
            }
            if (json.contains("fields")) {
                for (const nlohmann::json &jsonEl : json["fields"]) {
                    fields.emplace_back(jsonEl);
                }
            }
        }

    };

}