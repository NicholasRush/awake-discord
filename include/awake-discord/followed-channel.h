#pragma once

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#followed-channel-object-followed-channel-structure
    struct FollowedChannel {

        std::string channel_id;

        std::string webhook_id;

        FollowedChannel() = default;

        FollowedChannel(const nlohmann::json &json) {
            channel_id = json["channel_id"].get<std::string>();
            webhook_id = json["webhook_id"].get<std::string>();
        }

    };

}