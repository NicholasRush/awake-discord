#pragma once

#include <awake-discord/user.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/interactions/slash-commands#message-interaction-object-message-interaction-structure
    //TODO is type really InteractionCallbackType?
    struct MessageInteraction {

        std::string id;

        InteractionCallbackType type;

        std::string name;

        User user;

        MessageInteraction() = default;

        MessageInteraction(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            type = InteractionCallbackType(json["type"].get<int>());
            name = json["name"].get<std::string>();
            user = User(json["user"]);
        }

    };

}