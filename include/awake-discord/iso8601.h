#pragma once

#include <ctime>
#include <string>

namespace AwakeDiscord {

    struct ISO8601 {
        static time_t fromTimestamp(std::string str);
    };

}