#pragma once

#include <awake-discord/overwrite.h>
#include <awake-discord/user.h>
#include <awake-discord/thread-metadata.h>
#include <awake-discord/thread-member.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#channel-object-channel-structure
    struct Channel {

        std::string id;

        ChannelType type;

        std::optional<std::string> guild_id;

        std::optional<int> position;

        std::vector<Overwrite> permission_overwrites;

        std::optional<std::string> name;

        std::optional<std::string> topic;

        std::optional<bool> nsfw;

        std::optional<std::string> last_message_id;

        std::optional<int> bitrate;

        std::optional<int> user_limit;

        std::optional<int> rate_limit_per_user;

        std::vector<User> recipients;

        std::optional<std::string> icon;

        std::optional<std::string> owner_id;

        std::optional<std::string> application_id;

        std::optional<std::string> parent_id;

        std::optional<time_t> last_pin_timestamp;

        std::optional<std::string> rtc_region;

        std::optional<VideoQualityMode> video_quality_mode;

        std::optional<int> message_count;

        std::optional<int> member_count;

        std::optional<ThreadMetadata> thread_metadata;

        std::optional<ThreadMember> member;

        std::optional<int> default_auto_archive_duration;

        Channel() = default;

        Channel(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            type = ChannelType(json["type"].get<int>());
            if (json.contains("guild_id")) {
                guild_id = json["guild_id"].get<std::string>();
            }
            if (json.contains("position")) {
                position = json["position"].get<int>();
            }
            if (json.contains("permission_overwrites")) {
                for (const nlohmann::json &jsonEl : json["permission_overwrites"]) {
                    permission_overwrites.emplace_back(jsonEl);
                }
            }
            if (json.contains("name")) {
                name = json["name"].get<std::string>();
            }
            if (json.contains("topic")) {
                topic = json["topic"].get<std::string>();
            }
            if (json.contains("nsfw")) {
                nsfw = json["nsfw"].get<bool>();
            }
            if (json.contains("last_message_id") && !json["last_message_id"].is_null()) {
                last_message_id = json["last_message_id"].get<std::string>();
            }
            if (json.contains("bitrate")) {
                bitrate = json["bitrate"].get<int>();
            }
            if (json.contains("user_limit")) {
                user_limit = json["user_limit"].get<int>();
            }
            if (json.contains("rate_limit_per_user")) {
                rate_limit_per_user = json["rate_limit_per_user"].get<int>();
            }
            if (json.contains("recipients")) {
                for (const nlohmann::json &jsonEl : json["recipients"]) {
                    recipients.emplace_back(jsonEl);
                }
            }
            if (json.contains("icon") && !json["icon"].is_null()) {
                icon = json["icon"].get<std::string>();
            }
            if (json.contains("owner_id")) {
                owner_id = json["owner_id"].get<std::string>();
            }
            if (json.contains("application_id")) {
                application_id = json["application_id"].get<std::string>();
            }
            if (json.contains("parent_id") && !json["parent_id"].is_null()) {
                parent_id = json["parent_id"].get<std::string>();
            }
            if (json.contains("last_pin_timestamp") && !json["last_pin_timestamp"].is_null()) {
                last_pin_timestamp = ISO8601::fromTimestamp(json["last_pin_timestamp"].get<std::string>());
            }
            if (json.contains("rtc_region") && !json["rtc_region"].is_null()) {
                rtc_region = json["rtc_region"].get<std::string>();
            }
            if (json.contains("video_quality_mode")) {
                video_quality_mode = VideoQualityMode(json["video_quality_mode"].get<int>());
            }
            if (json.contains("message_count")) {
                message_count = json["message_count"].get<int>();
            }
            if (json.contains("member_count")) {
                member_count = json["member_count"].get<int>();
            }
            if (json.contains("thread_metadata")) {
                thread_metadata = ThreadMetadata(json["thread_metadata"]);
            }
            if (json.contains("member")) {
                member = ThreadMember(json["member"]);
            }
            if (json.contains("default_auto_archive_duration")) {
                default_auto_archive_duration = json["default_auto_archive_duration"].get<int>();
            }
        }

    };

}