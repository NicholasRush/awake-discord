#pragma once

#include <awake-discord/role-tags.h>
#include <awake-discord/enums.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/permissions#role-object-role-structure
    struct PartialRole {

        std::string id;

        std::string name;

        PartialRole() = default;

        PartialRole(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
        }

    };

    struct Role : PartialRole {

        int color;

        bool hoist;

        int position;

        PermissionFlags permissions;

        bool managed;

        bool mentionable;

        std::optional<RoleTags> tags;

        Role() = default;

        Role(const nlohmann::json &json) : PartialRole(json) {
            color = json["color"].get<int>();
            hoist = json["hoist"].get<bool>();
            position = json["position"].get<int>();
            permissions = std::stoll(json["permissions"].get<std::string>());
            managed = json["managed"].get<bool>();
            mentionable = json["mentionable"].get<bool>();
            if (json.contains("tags")) {
                tags = RoleTags(json["tags"]);
            }
        }

    };

}