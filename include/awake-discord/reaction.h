#pragma once

#include <awake-discord/emoji.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#reaction-object-reaction-structure
    struct Reaction {

        int count;

        bool me;

        Emoji emoji;

        Reaction() = default;

        Reaction(const nlohmann::json &json) {
            count = json["count"].get<int>();
            me = json["me"].get<bool>();
            emoji = Emoji(json["emoji"]);
        }

    };

}