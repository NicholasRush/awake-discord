#pragma once

#include <awake-discord/iso8601.h>

#include <ctime>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/invite#invite-metadata-object-invite-metadata-structure
    struct InviteMetadata {

        int uses;

        int max_uses;

        int max_age;

        bool temporary;

        time_t created_at;

        InviteMetadata() = default;

        InviteMetadata(const nlohmann::json &json) {
            uses = json["uses"].get<int>();
            max_uses = json["max_uses"].get<int>();
            max_age = json["max_age"].get<int>();
            temporary = json["temporary"].get<bool>();
            created_at = ISO8601::fromTimestamp(json["created_at"].get<std::string>());
        }

    };

}