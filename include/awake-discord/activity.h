#pragma once

#include <awake-discord/emoji.h>
#include <awake-discord/activity-timestamps.h>
#include <awake-discord/activity-party.h>
#include <awake-discord/activity-assets.h>
#include <awake-discord/activity-secrets.h>
#include <awake-discord/activity-button.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-structure
    struct Activity {

        std::string name;

        ActivityType type;

        std::optional<std::string> url;

        time_t created_at;

        std::optional<ActivityTimestamps> timestamps;

        std::optional<std::string> application_id;

        std::optional<std::string> details;

        std::optional<std::string> state;

        std::optional<Emoji> emoji;

        std::optional<ActivityParty> party;

        std::optional<ActivityAssets> assets;

        std::optional<ActivitySecrets> secrets;

        std::optional<bool> instance;

        std::optional<ActivityFlags> flags;

        std::vector<ActivityButton> buttons;

        Activity() = default;

        Activity(const nlohmann::json &json) {
            name = json["name"].get<std::string>();
            type = ActivityType(json["type"].get<int>());
            if (json.contains("url") && !json["url"].is_null()) {
                url = json["url"].get<std::string>();
            }
            created_at = json["created_at"].get<int>();
            if (json.contains("timestamps")) {
                timestamps = ActivityTimestamps(json["timestamps"]);
            }
            if (json.contains("application_id")) {
                application_id = json["application_id"].get<std::string>();
            }
            if (json.contains("details") && !json["details"].is_null()) {
                details = json["details"].get<std::string>();
            }
            if (json.contains("state") && !json["state"].is_null()) {
                state = json["state"].get<std::string>();
            }
            if (json.contains("emoji") && !json["emoji"].is_null()) {
                emoji = Emoji(json["emoji"]);
            }
            if (json.contains("party")) {
                party = ActivityParty(json["party"]);
            }
            if (json.contains("assets")) {
                assets = ActivityAssets(json["assets"]);
            }
            if (json.contains("secrets")) {
                secrets = ActivitySecrets(json["secrets"]);
            }
            if (json.contains("instance")) {
                instance = json["instance"].get<bool>();
            }
            if (json.contains("flags")) {
                flags = ActivityFlags(json["flags"].get<int>());
            }
            if (json.contains("buttons")) {
                for (const nlohmann::json &jsonEl : json["buttons"]) {
                    buttons.emplace_back(jsonEl);
                }
            }
        }

    };

}