#pragma once

#include <awake-discord/user.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/teams#data-models-team-members-object
    struct TeamMember {

        MembershipState membership_state;

        std::vector<std::string> permissions;

        std::string team_id;

        User user;

        TeamMember() = default;

        TeamMember(const nlohmann::json &json) {
            membership_state = MembershipState(json["membership_state"].get<int>());
            for (const nlohmann::json &jsonEl : json["permissions"]) {
                permissions.push_back(jsonEl.get<std::string>());
            }
            team_id = json["team_id"].get<std::string>();
            user = User(json["user"]);
        }
    };

}