#pragma once

#include <bitflags/bitflags.hpp>
#include <vector>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#embed-object-embed-types
    enum class EmbedType {
        rich,
        image,
        video,
        gifv,
        article,
        link
    };

    struct EmbedType_Values {
    private:
        const static std::string values[];
    public:
        static EmbedType fromString(const std::string &str);
    };

    //https://discord.com/developers/docs/topics/gateway#presence-update-presence-update-event-fields
    enum class PresenceUpdateStatus {
        idle,
        dnd,
        online,
        offline
    };

    struct PresenceUpdateStatus_Values {
    private:
        const static std::string values[];
    public:
        static PresenceUpdateStatus fromString(const std::string &str);
    };

    //https://discord.com/developers/docs/topics/gateway#client-status-object
    enum class ClientStatusStatus {
        online,
        idle,
        dnd
    };

    struct ClientStatusStatus_Values {
    private:
        const static std::string values[];
    public:
        static ClientStatusStatus fromString(const std::string &str);
    };

    //https://discord.com/developers/docs/resources/guild#guild-object-guild-features
    BITFLAGS(GuildFeatureFlags, int,
             BITFLAG(1 << 0, ANIMATED_ICON)
             BITFLAG(1 << 1, BANNER)
             BITFLAG(1 << 2, COMMERCE)
             BITFLAG(1 << 3, COMMUNITY)
             BITFLAG(1 << 4, DISCOVERABLE)
             BITFLAG(1 << 5, FEATURABLE)
             BITFLAG(1 << 6, INVITE_SPLASH)
             BITFLAG(1 << 7, MEMBER_VERIFICATION_GATE_ENABLED)
             BITFLAG(1 << 8, NEWS)
             BITFLAG(1 << 9, PARTNERED)
             BITFLAG(1 << 10, PREVIEW_ENABLED)
             BITFLAG(1 << 11, VANITY_URL)
             BITFLAG(1 << 12, VERIFIED)
             BITFLAG(1 << 13, VIP_REGIONS)
             BITFLAG(1 << 14, WELCOME_SCREEN_ENABLED)
             BITFLAG(1 << 15, TICKETED_EVENTS_ENABLED)
             BITFLAG(1 << 16, MONETIZATION_ENABLED)
             BITFLAG(1 << 17, MORE_STICKERS)
    )

    struct GuildFeatureFlags_Values {
    private:
        const static std::string values[];
    public:
        static GuildFeatureFlags fromStringsVector(const std::vector<std::string> &strs);
        static std::vector<std::string> toStringsVector(GuildFeatureFlags input);
    };

    //https://discord.com/developers/docs/resources/guild#integration-object-integration-structure
    enum class IntegrationType {
        twitch,
        youtube,
        discord
    };

    struct IntegrationType_Values {
    private:
        const static std::string values[];
    public:
        static IntegrationType fromString(const std::string &str);
    };

    //https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key
    enum class AuditLogChangeKey {
        name,
        description,
        icon_hash,
        splash_hash,
        discovery_splash_hash,
        banner_hash,
        owner_id,
        region,
        preferred_locale,
        afk_channel_id,
        afk_timeout,
        rules_channel_id,
        public_updates_channel_id,
        mfa_level,
        verification_level,
        explicit_content_filter,
        default_message_notifications,
        vanity_url_code,
        $add,
        $remove,
        prune_delete_days,
        widget_enabled,
        widget_channel_id,
        system_channel_id,
        position,
        topic,
        bitrate,
        permission_overwrites,
        nsfw,
        application_id,
        rate_limit_per_user,
        permissions,
        color,
        hoist,
        mentionable,
        allow,
        deny,
        code,
        channel_id,
        inviter_id,
        max_uses,
        uses,
        max_age,
        temporary,
        deaf,
        mute,
        nick,
        avatar_hash,
        id,
        type,
        enable_emoticons,
        expire_behavior,
        expire_grace_period,
        user_limit,
        privacy_level,
        tags,
        format_type,
        asset,
        available,
        guild_id
    };

    struct AuditLogChangeKey_Values {
    private:
        const static std::string values[];
    public:
        static AuditLogChangeKey fromString(const std::string &str);
    };

    //https://discord.com/developers/docs/resources/user#connection-object-connection-structure
    enum class ConnectionService {
        twitch,
        youtube
    };

    struct ConnectionService_Values {
    private:
        const static std::string values[];
    public:
        static ConnectionService fromString(const std::string &str);
    };

    //https://discord.com/developers/docs/resources/channel#allowed-mentions-object-allowed-mention-types
    BITFLAGS(AllowedMentionTypeFlags, int,
             BITFLAG(1 << 0, RoleMentions)
             BITFLAG(1 << 1, UserMentions)
             BITFLAG(1 << 2, EveryoneMentions)
    )

    struct AllowedMentionTypeFlags_Values {
    private:
        const static std::string values[];
    public:
        static AllowedMentionTypeFlags fromStringsVector(const std::vector<std::string> &strs);
        static std::vector<std::string> toStringsVector(AllowedMentionTypeFlags input);
    };

    //https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object-optional-audit-entry-info
    enum class OptionalAuditEntryInfoType {
        role,
        member
    };

    struct OptionalAuditEntryInfoType_Values {
    private:
        const static std::string values[];
    public:
        static OptionalAuditEntryInfoType fromString(const std::string &str);
    };

}