#pragma once

#include <awake-discord/emoji.h>
#include <awake-discord/string-enums.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#guild-preview-object-guild-preview-structure
    struct GuildPreview {

        std::string id;

        std::string name;

        std::optional<std::string> icon;

        std::optional<std::string> splash;

        std::optional<std::string> discovery_splash;

        std::vector<Emoji> emojis;

        GuildFeatureFlags features;

        int approximate_member_count;

        int approximate_presence_count;

        std::optional<std::string> description;

        GuildPreview() = default;

        GuildPreview(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
            if (!json["icon"].is_null()) {
                icon = json["icon"].get<std::string>();
            }
            if (!json["splash"].is_null()) {
                splash = json["splash"].get<std::string>();
            }
            if (!json["discovery_splash"].is_null()) {
                discovery_splash = json["discovery_splash"].get<std::string>();
            }
            for (const nlohmann::json &jsonEl : json["emojis"]) {
                emojis.emplace_back(jsonEl);
            }
            std::vector<std::string> featuresValues;
            for (const nlohmann::json &jsonEl : json["features"]) {
                featuresValues.emplace_back(jsonEl.get<std::string>());
            }
            features = GuildFeatureFlags_Values::fromStringsVector(featuresValues);
            approximate_member_count = json["approximate_member_count"].get<int>();
            approximate_presence_count = json["approximate_presence_count"].get<int>();
            if (!json["description"].is_null()) {
                description = json["description"].get<std::string>();
            }
        }

    };

}