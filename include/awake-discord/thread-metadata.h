#pragma once

#include <awake-discord/iso8601.h>

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#thread-metadata-object-thread-metadata-structure
    struct ThreadMetadata {

        bool archived;

        int auto_archive_duration;

        time_t archive_timestamp;

        std::optional<bool> locked;

        ThreadMetadata() = default;

        ThreadMetadata(const nlohmann::json &json) {
            archived = json["archived"].get<bool>();
            auto_archive_duration = json["auto_archive_duration"].get<int>();
            archive_timestamp = ISO8601::fromTimestamp(json["archive_timestamp"].get<std::string>());
            if (json.contains("locked")) {
                locked = json["locked"].get<bool>();
            }
        }

    };

}