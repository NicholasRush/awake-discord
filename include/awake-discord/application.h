#pragma once

#include <awake-discord/team.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/application#application-object-application-structure
    struct Application {

        std::string id;

        std::string name;

        std::optional<std::string> icon;

        std::string description;

        std::vector<std::string> rpc_origins;

        bool bot_public;

        bool bot_require_code_grant;

        std::optional<std::string> terms_of_service_url;

        std::optional<std::string> privacy_policy_url;

        std::optional<User> owner;

        std::string summary;

        std::string verify_key;

        std::optional<Team> team;

        std::optional<std::string> guild_id;

        std::optional<std::string> primary_sku_id;

        std::optional<std::string> slug;

        std::optional<std::string> cover_image;

        std::optional<ApplicationFlags> flags;

        Application() = default;

        Application(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
            if (!json["icon"].is_null()) {
                icon = json["icon"].get<std::string>();
            }
            description = json["description"].get<std::string>();
            if (json.contains("rpc_origins")) {
                for (const nlohmann::json &jsonEl : json["rpc_origins"]) {
                    rpc_origins.push_back(jsonEl.get<std::string>());
                }
            }
            bot_public = json["bot_public"].get<bool>();
            bot_require_code_grant = json["bot_require_code_grant"].get<bool>();
            if (json.contains("terms_of_service_url")) {
                terms_of_service_url = json["terms_of_service_url"].get<std::string>();
            }
            if (json.contains("privacy_policy_url")) {
                privacy_policy_url = json["privacy_policy_url"].get<std::string>();
            }
            if (json.contains("owner")) {
                owner = User(json["owner"]);
            }
            summary = json["summary"].get<std::string>();
            verify_key = json["verify_key"].get<std::string>();
            if (!json["team"].is_null()) {
                team = Team(json["team"]);
            }
            if (json.contains("guild_id")) {
                guild_id = json["guild_id"].get<std::string>();
            }
            if (json.contains("primary_sku_id")) {
                primary_sku_id = json["primary_sku_id"].get<std::string>();
            }
            if (json.contains("slug")) {
                slug = json["slug"].get<std::string>();
            }
            if (json.contains("cover_image")) {
                cover_image = json["cover_image"].get<std::string>();
            }
            if (json.contains("flags")) {
                flags = ApplicationFlags(json["flags"].get<int>());
            }
        }

    };

}