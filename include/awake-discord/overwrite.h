#pragma once

#include <awake-discord/enums.h>

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#overwrite-object-overwrite-structure
    struct Overwrite {

        std::string id;

        OverwriteType type;

        PermissionFlags allow;

        PermissionFlags deny;

        Overwrite() = default;

        Overwrite(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            type = OverwriteType(json["type"].get<int>());
            allow = std::stoll(json["allow"].get<std::string>());
            deny = std::stoll(json["deny"].get<std::string>());
        }

    };

}