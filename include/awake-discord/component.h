#pragma once

#include <awake-discord/emoji.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/interactions/message-components#component-object
    struct Component {

        ComponentType type;

        std::optional<ButtonStyle> style;

        std::optional<std::string> label;

        std::optional<Emoji> emoji;

        std::optional<std::string> custom_id;

        std::optional<std::string> url;

        std::optional<bool> disabled;

        std::vector<Component> components;

        Component() = default;

        Component(const nlohmann::json &json) {
            type = ComponentType(json["type"].get<int>());
            if (json.contains("style")) {
                style = ButtonStyle(json["style"].get<int>());
            }
            if (json.contains("label")) {
                label = json["label"].get<std::string>();
            }
            if (json.contains("emoji")) {
                emoji = Emoji(json["emoji"]);
            }
            if (json.contains("custom_id")) {
                custom_id = json["custom_id"].get<std::string>();
            }
            if (json.contains("url")) {
                url = json["url"].get<std::string>();
            }
            if (json.contains("disabled")) {
                disabled = json["disabled"].get<bool>();
            }
            if (json.contains("components")) {
                for (const nlohmann::json &jsonEl : json["components"]) {
                    components.emplace_back(jsonEl);
                }
            }
        }

    };

}