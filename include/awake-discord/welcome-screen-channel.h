#pragma once

#include <nlohmann/json.hpp>
#include <optional>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#welcome-screen-object-welcome-screen-channel-structure
    struct WelcomeScreenChannel {

        std::string channel_id;

        std::string description;

        std::optional<std::string> emoji_id;

        std::optional<std::string> emoji_name;

        WelcomeScreenChannel() = default;

        WelcomeScreenChannel(const nlohmann::json &json) {
            channel_id = json["channel_id"].get<std::string>();
            description = json["description"].get<std::string>();
            if (!json["emoji_id"].is_null()) {
                emoji_id = json["emoji_id"].get<std::string>();
            }
            if (!json["emoji_name"].is_null()) {
                emoji_id = json["emoji_name"].get<std::string>();
            }
        }

    };

}