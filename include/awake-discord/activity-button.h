#pragma once

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-buttons
    struct ActivityButton {

        std::string label;

        std::string url;

        ActivityButton() = default;

        ActivityButton(const nlohmann::json &json) {
            label = json["label"].get<std::string>();
            url = json["url"].get<std::string>();
        }

    };

}