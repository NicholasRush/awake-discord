#pragma once

#include <awake-discord/user.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#integration-application-object-integration-application-structure
    struct IntegrationApplication {

        std::string id;

        std::string name;

        std::optional<std::string> icon;

        std::string description;

        std::string summary;

        std::optional<User> bot;

        IntegrationApplication() = default;

        IntegrationApplication(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
            if (!json["icon"].is_null()) {
                icon = json["icon"].get<std::string>();
            }
            description = json["description"].get<std::string>();
            summary = json["summary"].get<std::string>();
            if (json.contains("bot")) {
                bot = User(json["bot"]);
            }
        }

    };

}