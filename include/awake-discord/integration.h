#pragma once

#include <awake-discord/integration-account.h>
#include <awake-discord/integration-application.h>
#include <awake-discord/string-enums.h>
#include <awake-discord/iso8601.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#integration-object-integration-structure
    struct Integration {

        std::string id;

        std::string name;

        IntegrationType type;

        bool enabled;

        std::optional<bool> syncing;

        std::optional<std::string> role_id;

        std::optional<bool> enable_emoticons;

        std::optional<IntegrationExpireBehavior> expire_behavior;

        std::optional<int> expire_grace_period;

        std::optional<User> user;

        IntegrationAccount account;

        std::optional<time_t> synced_at;

        std::optional<int> subscriber_count;

        std::optional<bool> revoked;

        std::optional<IntegrationApplication> application;

        Integration() = default;

        Integration(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
            type = IntegrationType_Values::fromString(json["type"].get<std::string>());
            enabled = json["enabled"].get<bool>();
            if (json.contains("syncing")) {
                syncing = json["syncing"].get<bool>();
            }
            if (json.contains("role_id")) {
                role_id = json["role_id"].get<std::string>();
            }
            if (json.contains("enable_emoticons")) {
                enable_emoticons = json["enable_emoticons"].get<bool>();
            }
            if (json.contains("expire_behavior")) {
                expire_behavior = IntegrationExpireBehavior(json["expire_behavior"].get<int>());
            }
            if (json.contains("expire_grace_period")) {
                expire_grace_period = json["expire_grace_period"].get<int>();
            }
            if (json.contains("user")) {
                user = User(json["user"]);
            }
            if (json.contains("account")) {
                account = IntegrationAccount(json["account"]);
            }
            if (json.contains("synced_at")) {
                synced_at = ISO8601::fromTimestamp(json["synced_at"].get<std::string>());
            }
            if (json.contains("subscriber_count")) {
                subscriber_count = json["subscriber_count"].get<int>();
            }
            if (json.contains("revoked")) {
                revoked = json["revoked"].get<bool>();
            }
            if (json.contains("application")) {
                application = IntegrationApplication(json["application"]);
            }
        }

    };

}